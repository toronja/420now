const rp = require('request-promise');

class Geocoding {

    constructor(apiKey) {
        this.apiKey = apiKey;
    }

    async fetchData(locationData = {}) {
        const {address, city, state, zipcode} = locationData;
        let result = await this.Get(locationData).then((result) => { return JSON.parse(result) });
        return result.results;
    }

    Get(qs = {}) {
        let config = {
            url: "https://maps.googleapis.com/maps/api/geocode/json",
            method: "get",
            qs: Object.assign({ key: this.apiKey }, qs)
        }
        return rp(config);
    }
}
export default Geocoding;