import React from 'react';
import './App.css';
import './Spinner.css';
import Timezone from './Timezone';
import GoogleMaps from './GoogleMaps';
import { LocationProvider }  from './LocationProvider';

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      title: "420 now",
      isLoading: true
    }
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1>{this.state.title}</h1>
        </header>
        <div className="Body">
          <LocationProvider>
            <Timezone className="timezone-component" />
            <GoogleMaps locationList />
          </LocationProvider>
        </div>
      </div >
    )
  }
}


export default App;
