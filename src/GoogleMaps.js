import React, { Component } from 'react';
import GoogleMapReact from 'google-map-react';
import TimezoneFetcher from './TimezoneFetcher';
import Geocoding from './Geocoding';
import './Marker.css'
const GOOGLE_API_KEY = process.env.REACT_APP_GOOGLE_API_KEY;
const retryms = 300;

const AnyReactComponent = ({ text }) => <div className="google-maps-marker"><p>{text}</p></div>;

class Location {

    constructor(name, lat, long) {
        this.name = name;
        this.lat = lat;
        this.long = long
    }
}

class SimpleMap extends Component {

    constructor() {
        super();
        this.timezoneFetcher = new TimezoneFetcher();
        this.geocoding = new Geocoding(GOOGLE_API_KEY);
        let locations = [];
        let times420 = this.timezoneFetcher.getTimes420();
        this.state = {
            locations,
            times420
        }
        setInterval(() => {
            this.setState({
                times420: this.timezoneFetcher.getTimes420()
            });
        }, 100);
    }

    async wait(ms) {
        return new Promise(resolve => {
            setTimeout(resolve, ms);
        });
    }

    async componentDidMount() {

        let msInBetweenCalls = 200;

        if (!this.state.times420.length) return;
        let geocodingResults = await Promise.all(this.state.times420.map(async (time, ix) => {
            let targetSplit = time._z.name.replace(/[_]/g, " ").split('/');
            let targetAddress = targetSplit.length > 2 ? `${targetSplit[2]}, ${targetSplit[1]}` : `${targetSplit[1]}`;
            let geocodingTimeoutCall = await new Promise((resolve) => {
                setTimeout(async () => {
                    let geocodingResult = await this.geocoding.fetchData({ address: targetAddress });
                    resolve(geocodingResult);
                }, msInBetweenCalls * ix);
            });
            return geocodingTimeoutCall;
        })).then(function () {
            this.setState({
                locations: geocodingResults
            }); 
        })
    }

    static defaultProps = {
        center: {
            lat: 59.95,
            lng: 30.33
        },
        zoom: 2
    };

    LoadMarkers() {
        if (!this.state.locations.length) return;

        return this.state.locations.map((locationArray, key) => {
            let location = locationArray[0];
            if(!location) return;
            let keyValue = `${location.formatted_address}-${key}`;
            return (
                <AnyReactComponent
                    key={keyValue}
                    text={location.formatted_address}
                    lat={location.geometry.location.lat}
                    lng={location.geometry.location.lng}
                />
            );
        });
    }

    handleApiLoaded = (map, maps) => {
        // use map and maps objects
    };

    render() {
        return (
            <div style={{ width: '50%' }}>
                <GoogleMapReact
                    bootstrapURLKeys={{ key: GOOGLE_API_KEY }}
                    defaultCenter={this.props.center}
                    defaultZoom={this.props.zoom}
                    yesIWantToUseGoogleMapApiInternals
                    onGoogleApiLoaded={({ map, maps }) => this.handleApiLoaded(map, maps)}
                >
                {this.LoadMarkers()}
                </GoogleMapReact>
            </div>
        );
    }
}

export default SimpleMap;