import React from 'react';

const LocationContext = React.createContext();

class LocationProvider extends React.Component {
    state = { message: "" }
    render() {
        return (
            <LocationContext.Provider value={
                {
                    state: this.state,
                    setMessage: (value) => this.setState({
                        message: value
                    })
                }}>
                {this.props.children}
            </LocationContext.Provider>)
    }
}

export {
    LocationProvider, LocationContext
}