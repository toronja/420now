import moment from 'moment-timezone';

class TimezoneFetcher {

    constructor() {
        this.timezones = moment.tz.names();
        this.times420 = [];
    }

    getTimes420() {
        let getAllCurrentTimes = function (timezones) {
            return timezones.map((timezone) => { return moment.tz(timezone); });
        };

        return getAllCurrentTimes(this.timezones).filter((element) => {
            if (element.hour() >= 15) {
                if (element.hour() > 16) return false;
                if (element.hour() === 16 && element.minute() > 20) return false;
                return element;
            }
        });
    }

}

export default TimezoneFetcher;