import React from 'react';
import Card from '@material-ui/core/Card';
import './Timezone.css';
import TimezoneFetcher from './TimezoneFetcher'
import { LocationContext } from './LocationProvider';

const timeDifference = 1;

const FlagIcon = ({ className }) => <div className="flag"></div>;

class Timezone extends React.Component {
    constructor() {
        super();
        this.fetcher = new TimezoneFetcher();
        this.state = {
            title: "Moment - Timezone",
            isLoading: true,
            times420: null
        }
        setInterval(() => {
            this.setState({
                times420: this.fetcher.getTimes420()
            });
        }, 100);
    }

    renderTimes() {
        if (this.state.times420) {
            let allTimes = this.state.times420.map((element) => {
                return { timezone: element._z.name, time: element.format('DD/MM/YYYY HH:mm:ss') };
            });

            return allTimes.map((element, key) => {
                let keyValue = `${element.time}-${key}`;
                return (
                    <Card className="timezone-card" variant="contained" color="primary" key={keyValue}>
                        <p>{element.time}<br/>
                        {element.timezone}</p>
                    </Card>
                );
            });
        }
        return (<p>Loading</p>);
    }

    render() {
        return (
            <div className="timezone-container">
                {this.renderTimes()}
            </div>
        )
    }
}

export default Timezone